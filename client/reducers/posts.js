function posts(state = [], action) {
    switch (action.type){
        case 'INCREMENT_LIKES':
            console.log('INCREMENT_LIKES');
            const i = action.index;
            return [
                ...state.slice(0, i),
                {...state[i], likes: state[i].likes + 1},
                ...state.slice(i + 1),
            ];
        case 'FETCH_POSTS_END':
            console.log('reducer: FETCH_POSTS_END');
            return [...action.posts];
        default:
            return state;
    }
}

export default posts;
