function postComments(state = [], action) {
    console.log('postComments');
    switch (action.type){
        case 'ADD_COMMENT':
            return [...state, {
                'user': action.author,
                'text': action.comment
            }];
        case 'REMOVE_COMMENT':
            return [
                ...state.slice(0, action.index),
                ...state.slice(action.index +1)
            ];
        case 'FETCH_COMMENTS_END':
            console.log('FETCH_COMMENTS_END');
            return [...action.comments];
        default:
            return state;
    }
    return state;
}

export default postComments;
