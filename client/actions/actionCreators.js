export function increment(index) {
    return {
        type: 'INCREMENT_LIKES',
        index
    }
}

export function addComment(postId, author, comment) {
    return {
        type: 'ADD_COMMENT',
        postId,
        author,
        comment
    }
}

export function removeComment(postId, index) {
    return {
        type: 'REMOVE_COMMENT',
        postId,
        index
    }
}

export function fetchCommentsStart() {
    console.log('fetchCommentsStart');
    return{
        type: 'FETCH_COMMENTS_START'
    }
}

export function fetchCommentsEnd(comments) {
    console.log('fetchCommentsEnd');
    return{
        type: 'FETCH_COMMENTS_END',
        comments
    }
}

export function fetchPostsStart() {
    console.log('fetchPostsStart');
    return{
        type: 'FETCH_POSTS_START'
    }
}

export function fetchPostsEnd(posts) {
    console.log('fetchPostsEnd');
    return{
        type: 'FETCH_POSTS_END',
        posts
    }
}
