import axios from 'axios';

export default function fetchPosts() {
    return axios
        .get('http://localhost:8083/api/posts')
        .then(response => response.data)
        .catch(error => {
            console.log('error');
            console.log(error);
        });
}
