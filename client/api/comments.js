import axios from 'axios';

export default function fetchComments() {
    return axios
        .get('http://localhost:8083/api/comments')
        .then(response => response.data)
        .catch(error => {
            console.log('error');
            console.log(error);
        });
}
