import {call, takeEvery, put} from 'redux-saga/effects';
import axios from 'axios';

import fetchPostsApi from './api/posts';
import fetchCommentsApi from './api/comments';
import {fetchPostsEnd, fetchCommentsEnd} from './actions/actionCreators';

function* fetchPosts(action) {
    console.log('saga: fetchPosts');
    try {const comments = yield call(fetchCommentsApi);
        const posts = yield call(fetchPostsApi);
        yield put(fetchCommentsEnd(comments));
        yield put(fetchPostsEnd(posts));
    } catch (e) {
        console.log(e.message);
    }
}

function* mySaga() {
    console.log('mySaga');
    yield takeEvery('FETCH_POSTS_START', fetchPosts);
}

export default mySaga;
