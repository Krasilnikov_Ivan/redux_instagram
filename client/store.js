import {createStore, compose, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import {syncHistoryWithStore} from 'react-router-redux';
import {browserHistory} from 'react-router';

// import rood reducer
import rootReducer from './reducers/index';
import mySaga from './sagas';

// create an object for the default data
const defaultState = {
    comments: [],
    posts: []
};

const enhantcers = compose(
    window.devToolsExtension ? window.devToolsExtension() : f => f
);

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
    rootReducer,
    defaultState,
    compose(
        applyMiddleware(sagaMiddleware),
        enhantcers
    )
);

sagaMiddleware.run(mySaga);

export const history = syncHistoryWithStore(browserHistory, store);

if(module.hot){
    module.hot.accept('./reducers', () => {
        const nextRootReducer = require('./reducers/index').default;
        store.replaceReducer(nextRootReducer);
    });
}

export default store;
