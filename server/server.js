var path = require('path');
var express = require('express');
var webpack = require('webpack');
var config = require('../webpack.config.dev');

var app = express();
var compiler = webpack(config);

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'X-Requested-With');
    next();
});

app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: config.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

app.get('/api/posts', function(req, res) {
    res.sendFile(path.join(__dirname + '/data', 'posts.json'));
});

app.get('/api/comments', function(req, res) {
    res.sendFile(path.join(__dirname + '/data', 'comments.json'));
});

app.listen(8083, 'localhost', function(err) {
    if (err) {
        console.log(err);
        return;
    }

    console.log('Server run');
});

app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function (err, req, next) {
    res.status(err.status() || 500);
    req.json({
        message: err.message,
        error: err
    })
});
